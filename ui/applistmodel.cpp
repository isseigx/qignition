#include "applistmodel.h"
#include <QIcon>
#include <QMimeData>
#include <QDebug>

AppListModel::AppListModel(QObject *parent) :
QAbstractListModel(parent)
{
}

QVariant AppListModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() > applist.count()) {
        return QVariant();
    }
    const Application &app = applist.at(index.row());
    
    if (role == Qt::DecorationRole) {
        const QStringList name = app.command.split(QLatin1String(" "));
        return QIcon::fromTheme(name.value(0, QLatin1String("application-x-executable")),
            QIcon::fromTheme(QLatin1String("application-x-executable")));
    }
    
    if (role == Qt::DisplayRole) {
        return QStringLiteral("%1 (%2)").arg(app.name, app.command);
    }
    
    if (role == NameRole)
        return app.name;
    else if (role == CommandRole)
        return app.command;
    
    return QVariant();
}

int AppListModel::rowCount(const QModelIndex &parent = QModelIndex()) const
{
    return parent.isValid() ? 0 : applist.count();
}

Qt::DropActions AppListModel::supportedDropActions() const
{
    return Qt::CopyAction | Qt::MoveAction;
}

void AppListModel::update()
{
    beginResetModel();
    endResetModel();
}

bool AppListModel::removeRows(int row, int count, const QModelIndex &parent = QModelIndex())
{
    /*int last = row + count - 1;
	beginRemoveRows(parent, row, last);
	
	for (int pos = 0; pos < count; ++pos) {
         applist.removeAt(pos);
     }
	
	endRemoveRows();
	qDebug() << "Removing";
	return false;*/
    return QAbstractItemModel::removeRows(row, count, parent);
}

Qt::ItemFlags AppListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
         return Qt::ItemIsEnabled | Qt::ItemIsDropEnabled;
 
    return QAbstractListModel::flags(index) | Qt::ItemIsDragEnabled;
}

QStringList AppListModel::mimeTypes() const
{
	return QStringList() << "application/vnd.text.list" << "text/uri-list";
}

QMimeData* AppListModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = new QMimeData();
	QByteArray encodedData;
	
	QDataStream stream(&encodedData, QIODevice::WriteOnly);
	
	foreach (const QModelIndex &index, indexes) {
		if (index.isValid()) {
			stream << applist.at(index.row()).command;
		}
	}
	
	mimeData->setData("application/vnd.text.list", encodedData);
	return mimeData;
}

bool AppListModel::canDropMimeData(const QMimeData *data, 
									Qt::DropAction action, int row, int column,
									const QModelIndex &parent)
{
	Q_UNUSED(action);
    Q_UNUSED(row);
    Q_UNUSED(parent);
	Q_UNUSED(column);
	
	if (!data->hasFormat("application/vnd.text.list"))
		return false;
	
	return true;
}

bool AppListModel::dropMimeData(const QMimeData *data,
                                Qt::DropAction action, int row,
                                int column, const QModelIndex &parent)
{
    if (!canDropMimeData(data, action, row, column, parent))
        return false;
    
    if (action == Qt::IgnoreAction)
        return true;
    
    int beginRow = 0;

    if (!parent.isValid() && row < 0)
        beginRow = applist.count() - 1;
    else if (!parent.isValid())
        beginRow = qMin(row, (applist.count() - 1));
    else
        beginRow = parent.row();


    QByteArray encodedData = data->data("application/vnd.text.list");
    QDataStream stream(&encodedData, QIODevice::ReadOnly);
    
    QString command;
    stream >> command;
    
    for (int i = 0; i < applist.count(); i++) {
        if (applist.at(i).command == command) {
            move(i, beginRow);
            break;
        }
    }
    
    //update();
    
    return true;
}

void AppListModel::setAppList(const QList<Application> &list)
{
    applist = list;
    update();
}

void AppListModel::remove(int index)
{
    if (index >= 0 || index < applist.count()) {
        beginRemoveRows(QModelIndex(), index, index);
        applist.removeAt(index);
        endRemoveRows();
    }
}

void AppListModel::add(const QString &name, const QString &command)
{
    int index = applist.count();
    Application app { name, command };
    applist.append(app);
    beginInsertRows(QModelIndex(), index, index);
    endInsertRows();
}

void AppListModel::edit(int pos, const QString &name, const QString &command)
{
    Application app { name, command };
    applist.replace(pos, app);
    emit dataChanged(index(pos), index(pos));
}

QHash<int, QByteArray> AppListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = QByteArrayLiteral("name");
    roles[CommandRole] = QByteArrayLiteral("command");
    return roles;
}

void AppListModel::read()
{
    IO io;
    applist = io.read();
    update();
}

void AppListModel::write()
{
    IO io;
    io.write(applist);
}

void AppListModel::move(int from, int to)
{
    if (from < 0 || from > applist.size())
        return;

    if (to < 0 || to > applist.size())
        return;
    
    if (from == to)
        return;
        
    beginMoveRows(QModelIndex(), from, from, QModelIndex(), to);
    applist.move(from, to);
    endMoveRows();
}