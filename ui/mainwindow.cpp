#include "ui_mainwindow.h"
#include "mainwindow.h"
#include "applistmodel.h"

#include <QCloseEvent>
#include <QInputDialog>
#include <QSettings>

MainWindow::MainWindow(QWidget *parent) : QWidget(parent),
ui(new Ui_MainWindow),
appindex(-1)
{
    ui->setupUi(this);
    setupAppListView();
    connectSignals();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setupAppListView()
{
    applistmodel = new AppListModel();
    applistmodel->read();
    ui->appListView->setModel(applistmodel);
}

void MainWindow::connectSignals()
{
    connect(ui->intervalPb, &QPushButton::clicked, this, &MainWindow::selectInterval);
    connect(ui->addPb, &QPushButton::clicked, this, &MainWindow::addApplication);
    connect(ui->editPb, &QPushButton::clicked, this, &MainWindow::editApplication);
    connect(ui->removePb, &QPushButton::clicked, this, &MainWindow::removeApplication);
    
    connect(ui->cancelPb, &QPushButton::clicked, [=]() {
        ui->stackedWidget->setCurrentIndex(0);
    });
    
    connect(ui->okPb, &QPushButton::clicked, this, &MainWindow::saveApplication);
}

void MainWindow::selectInterval()
{
    QSettings s;
    int interval = s.value(QStringLiteral("Delay"), 2000).toInt();
    
    bool ok;
    interval = QInputDialog::getInt(this, tr("Interval (seconds)"), tr("Interval between app launch"),
        interval / 1000, 0, 60, 1, &ok);
    
    if (ok) {
        s.setValue(QStringLiteral("Delay"), interval * 1000);
    }
}

void MainWindow::addApplication()
{
    clearLineEdit();
    ui->editAppLabel->setText(tr("Add application"));
    ui->stackedWidget->setCurrentIndex(1);
    editing = false;
}

void MainWindow::editApplication()
{
    const QModelIndex &index = ui->appListView->currentIndex();
    if (!index.isValid()) return;
    
    clearLineEdit();
    ui->editAppLabel->setText(tr("Edit application"));
    ui->stackedWidget->setCurrentIndex(1);
    
    const Application &app = applistmodel->applicationAt(index);
    ui->nameLineEdit->setText(app.name);
    ui->commandLineEdit->setText(app.command);
    
    editing = true;
    appindex = index.row();
}

void MainWindow::clearLineEdit()
{
    ui->nameLineEdit->clear();
    ui->commandLineEdit->clear();
}

void MainWindow::saveApplication()
{
    const QString &name = ui->nameLineEdit->text();
    const QString &command = ui->commandLineEdit->text();
    
    if (name.isEmpty() || command.isEmpty()) {
        return;
    }
    
    if (editing) {
        applistmodel->edit(appindex, name, command);
    } else {
        applistmodel->add(name, command);
    }
    
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::removeApplication()
{
    if (!ui->appListView->currentIndex().isValid()) return;
    
    applistmodel->remove(ui->appListView->currentIndex().row());
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    applistmodel->write();
    event->accept();
}
