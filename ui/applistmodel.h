#ifndef APPLISTMODEL_H
#define APPLISTMODEL_H

#include <QAbstractListModel>
#include "core/io.h"

class AppListModel : public QAbstractListModel
{
    Q_OBJECT
    
public:
    
    enum ApplicationRoles {
        NameRole = Qt::UserRole + 1,
        CommandRole
    };
    
    explicit AppListModel(QObject *parent = nullptr);
    
    QList<Application> appList() const { return applist; }
    void setAppList(const QList<Application> &);
    
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex&) const;
    Qt::DropActions supportedDropActions() const;
    bool removeRows(int, int, const QModelIndex &);
    Qt::ItemFlags flags(const QModelIndex &) const;
    QStringList mimeTypes() const;
    QMimeData* mimeData(const QModelIndexList &) const;
    bool canDropMimeData(const QMimeData *, Qt::DropAction, int, int, const QModelIndex &);
    bool dropMimeData(const QMimeData *, Qt::DropAction, int, int, const QModelIndex &);

    Q_INVOKABLE void remove(int);
    Q_INVOKABLE void add(const QString &, const QString &);
    Q_INVOKABLE void edit(int, const QString &, const QString &);
    Q_INVOKABLE void write();
    Q_INVOKABLE void read();
    Q_INVOKABLE void move(int, int);
    Application applicationAt(const QModelIndex &mindex) const { return applicationAt(mindex.row()); }
    Application applicationAt(int index) const { return applist.at(index); }

public slots:
    void update();
    
protected:
    QHash<int, QByteArray> roleNames() const;
    
private:
    QList<Application> applist;
};

#endif //APPLISTMODEL_H
