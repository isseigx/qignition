#include "ui/mainwindow.h"

#include <QApplication>
#include <QIcon>

int main (int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName(QStringLiteral("qIgnition"));
    app.setOrganizationName(QStringLiteral("DreamState"));
    app.setOrganizationDomain(QStringLiteral("dreamstate.jp"));
    app.setWindowIcon(QIcon::fromTheme(QStringLiteral("qignition"), 
        QIcon(QStringLiteral(":/icons/qignition.svg"))));
    
    MainWindow mw;
    mw.show();
    
    return app.exec();
}
