#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>

class Ui_MainWindow;
class AppListModel;
class QCloseEvent;

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void selectInterval();
    void addApplication();
    void editApplication();
    void saveApplication();
    void removeApplication();

protected:
    void closeEvent(QCloseEvent *) override;
    
private:
    Q_DISABLE_COPY(MainWindow)
    void connectSignals();
    void setupAppListView();
    void clearLineEdit();
    
    Ui_MainWindow *ui;
    AppListModel *applistmodel;
    bool editing;
    int appindex;
};

#endif // MAINwINDOW_H
