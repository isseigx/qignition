#include "core/launchapps.h"

#include <QCoreApplication>

int main (int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    app.setApplicationName(QStringLiteral("qIgnition"));
    app.setOrganizationName(QStringLiteral("DreamState"));
    app.setOrganizationDomain(QStringLiteral("dreamstate.jp"));
    
    LaunchApps launchApps;
    QObject::connect(&launchApps, &LaunchApps::exitNow, &app, &QCoreApplication::quit);
    launchApps.launchApps();
    
    return app.exec();
}
