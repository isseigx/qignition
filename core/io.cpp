#include "io.h"

#include <QDebug>
#include <QDir>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QStandardPaths>
#include <QTextStream>

QString IO::dataLocation() const
{
    const QString folder = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).first();
    QDir dir;
    
    if (!dir.exists(folder)) {
        dir.mkpath(folder);
    }
    
    return (folder + QStringLiteral("/apps.json"));
}

QList<Application> IO::read() const
{
    QList<Application> applist;
    QFile file(dataLocation());
    
    if (!file.exists()) {
        qCritical("[IO] Applications file does not exists");
        return applist;
    }
    
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qCritical("[IO] Cannot open applications file");
        return applist;
    }
    
    QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
    QJsonObject obj = doc.object();
    QJsonArray array = obj[QLatin1String("apps")].toArray();
    
    for (auto item : qAsConst(array)) {
        const QJsonObject obj = item.toObject();
        Application app {obj[QLatin1String("name")].toString(), obj[QLatin1String("command")].toString()};
        applist.append(app);
    }
    
    return applist;
}

bool IO::write(const QList<Application> &applist)
{
    if (applist.isEmpty()) {
        qWarning("[IO] Application list is empty");
        return false;
    }
    
    QJsonArray array;
    
    for (const Application &app : qAsConst(applist)) {
        QJsonObject objtmp;
        objtmp[QLatin1String("name")] = app.name;
        objtmp[QLatin1String("command")] = app.command;
        array.append(objtmp);
    }
    
    QJsonObject object;
    object[QLatin1String("apps")] = array;
    
    QFile file(dataLocation());
    
    
    if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QTextStream out(&file);
        out << QJsonDocument(object).toJson();
        return true;
    }
    
    qCritical("[IO] Cannot write file");
    return false;
}
