#include "launchapps.h"

#include <QDir>
#include <QProcess>
#include <QSettings>
#include <QTimer>

LaunchApps::LaunchApps()
{
    timer = new QTimer(this);
    connectActions();
}

LaunchApps::~LaunchApps()
{
    delete timer;
}

void LaunchApps::launchApps()
{
    //read settings
    QSettings s;
    int delay = s.value(QStringLiteral("Delay"), 2000).toInt();
    
    IO io;
    apps = io.read();

    //if list is empty, return
    if(apps.isEmpty())
        return;

    //start timer
    if(delay == 0)
        timer->start(500);
    else
        timer->start(delay);
	
}

void LaunchApps::connectActions()
{
    connect(timer, &QTimer::timeout, this, &LaunchApps::init);
}

void LaunchApps::init()
{
    if(apps.isEmpty()) {
        timer->stop();
        emit exitNow();
        return;
    }
    
    QStringList args = apps.first().command.split(QLatin1String(" "));
    QProcess::startDetached(args.takeFirst(), args);
    apps.removeFirst();
}