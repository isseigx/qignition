#ifndef IO_H
#define IO_H

#include <QList>
#include <QString>

struct Application {
    QString name;
    QString command;
};

class IO
{
public:
    IO() = default;

    QList<Application> read() const;
    bool write(const QList<Application> &);
    
    QString dataLocation() const;
};

#endif //IO_H
