#ifndef LAUNCHAPPS_H
#define LAUNCHAPPS_H

#include <QObject>

#include "io.h"

class QTimer;

class LaunchApps : public QObject
{
	Q_OBJECT
public:
    explicit LaunchApps();
    ~LaunchApps();

    void launchApps();
	
signals:
    void exitNow();
	
private slots:
    void init();
	
private:
    void connectActions();
    QList<Application> apps;
    QTimer *timer;
};

#endif //LAUNCHAPPS_H
