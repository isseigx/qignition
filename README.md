## qIgnition

qIgnition is a small program to launch apps on start up.

It was created to launch applications in window managers that does not have a graphical way to launch application on start up.

Currently it saves all commands in a JSON file but in the future its going to change to FreeDesktop standards (read and write to desktop files in $HOME/.config/autostart and read from /etc/xdg/autostart).

### Usage
- Start ```qignition_ui``` from your menu or terminal emulator and add your apps.
- Add ```qignition``` to your window manager/compositor exec method (eg. openbox in ```$HOME/.config/openbox/autostart```, sway in ```$HOME/.config/sway/config```)

### Dependencies
- ```qt5-base``` (build and runtime)
- ```cmake``` (build)
- ```qt5-svg``` (runtime, for svg icons)